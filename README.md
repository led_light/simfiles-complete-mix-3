"led_light Complete Mix 3" simfile pad pack by led_light for StepMania/Project OutFox.

To download the pack or for more information, see the [simfile meta-repository](https://gitlab.com/led_light/simfiles) or my simfile home page on [Zenius -I- vanisher.com](https://zenius-i-vanisher.com/v5.2/viewsimfilecategory.php?categoryid=821).

All music, videos, and graphics are copyright of their respective owners.
